package getSearchResultsClient;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class GetSearchProducts {
    public Response getSearchProducts(String queryParam,String searchApi){
        Response searchProducts=given()
                .contentType(ContentType.JSON)
                .queryParam("k",queryParam)
                .when()
                .get(searchApi);
        return searchProducts;
    }
}
