package postUserDetailsClient;

import com.google.gson.Gson;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import postUserDetailsClient.request.PostUserDetailsRequest;
import postUserDetailsClient.response.PostUserDetailsResponse;

import static io.restassured.RestAssured.given;

public class PostUserDetails {
    public PostUserDetailsResponse postUserDetails(PostUserDetailsRequest postUserDetailsRequest, String postUsersApi){
        Response userDetails=given()
                .when()
                .contentType(ContentType.JSON)
                .body(new Gson().toJson(postUserDetailsRequest))
                .post(postUsersApi);
        return new Gson().fromJson(userDetails.body().asString(),PostUserDetailsResponse.class);
    }
}
