import getSearchResultsClient.GetSearchProducts;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class GETResponse {
    @Test
    public void shoulBeAbleToGetSearchResults(){
        GetSearchProducts getSearchProducts=new GetSearchProducts();
        Response searchResponse;
        searchResponse=getSearchProducts.getSearchProducts("shirts","https://www.amazon.in/s");
        Assert.assertTrue(searchResponse.getStatusCode()==200);
    }
}
