import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import postUserDetailsClient.PostUserDetails;
import postUserDetailsClient.request.PostUserDetailsRequest;
import postUserDetailsClient.response.PostUserDetailsResponse;

import static io.restassured.RestAssured.given;

public class POSTResponse {
    @Test
    public void shouldBeAbleToPOSTUserDetails(){
        PostUserDetailsRequest postUserDetailsRequest=new PostUserDetailsRequest();
        postUserDetailsRequest.setId("11");
        postUserDetailsRequest.setName("Emil");
        postUserDetailsRequest.setUsername("emilfranc");
        postUserDetailsRequest.setEmail("e@g.in");
        PostUserDetailsResponse postUserDetailsResponse= new PostUserDetails().postUserDetails(postUserDetailsRequest,"https://jsonplaceholder.typicode.com/users");
        Assert.assertTrue(postUserDetailsResponse.getEmail().equals("e@g.in"));
    }
}
